set autoindent
set tabstop=4
set shiftwidth=4
set number
set hlsearch

syntax on
colorscheme desert

map <F3> :tabp
map <F4> :tabn

"hack to keep trailing whitespace on empty lines
inoremap <CR> <CR>x<BS>
inoremap o ox<BS>

"delete trailing whitespace on save
autocmd BufWritePre * :%s/\s\+$//e

"treat json as javascript for highlighting
autocmd BufNewFile,BufRead *.json set ft=javascript

"treat less as css for highlighting
autocmd BufNewFile,BufRead *.less set ft=css

autocmd BufNewFile,BufRead *.py setlocal expandtab

"map greek characters to english ones for command mode
map Α A
map Β B
map Ψ C
map Δ D
map Ε E
map Φ F
map Γ G
map Η H
map Ι I
map Ξ J
map Κ K
map Λ L
map Μ M
map Ν N
map Ο O
map Π P
map Q Q
map Ρ R
map Σ S
map Τ T
map Θ U
map Ω V
map W W
map Χ X
map Υ Y
map Ζ Z

map α a
map β b
map ψ c
map δ d
map ε e
map φ f
map γ g
map η h
map ι i
map ξ j
map κ k
map λ l
map μ m
map ν n
map ο o
map π p
map q q
map ρ r
map σ s
map τ t
map θ u
map ω v
map ς w
map χ x
map υ y
map ζ z
