PATH="$HOME"/.local/bin:$PATH
PATH="$HOME"/dotfiles/bin:$PATH

uniqpath() {
	# I need to respect the PATH coming from the environment (because of wsl,
	# pew etc.) so I can't set PATH from scratch. Appending to the PATH leads
	# to duplicates in subshells, so I'll just dedupe the PATH. It's still not
	# perfetct since whatever I append has precedence over the environment.
	# The awk garbage is uniq without sorting
	tr : \\n | awk '!x[$0]++' | tr \\n : | sed -e 's/:$//'
}
 
PATH=$(echo "$PATH" | uniqpath)
