wgit() {
	watch --color git -c color.ui=always $@
}
compdef _git wgit=git

fixup() {
   git commit --fixup="$1" && git rebase -i "$1"~1
}

