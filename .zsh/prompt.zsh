autoload -Uz colors && colors
autoload -Uz vcs_info

# configure vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr ✘
zstyle ':vcs_info:*' formats "%{$fg_bold[blue]%}%s:(%{$fg_bold[red]%}%b%{$fg_bold[blue]%}) %{$fg_bold[yellow]%}%u%{$reset_color%}"
precmd () { vcs_info }

# virtualenv info helper
function _virtualenv_info {
	if [ $VIRTUAL_ENV ]; then
		echo "%{$fg_bold[blue]%}venv:(%{$fg[red]%}$(basename $VIRTUAL_ENV)%{$fg_bold[blue]%})%{$reset_color%}"
	fi
}

export VIRTUAL_ENV_DISABLE_PROMPT=1  # disable virtualenv's prompt since we use our own
PROMPT='[%{$fg_bold[red]%}%m%{$reset_color%} %c] '
RPROMPT='$(_virtualenv_info) ${vcs_info_msg_0_}'
