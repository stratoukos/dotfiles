export LANG=en_US.UTF-8

# history
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt INC_APPEND_HISTORY EXTENDED_HISTORY

# make up and down keys only search history matching the current prefix
bindkey '^[[A' up-line-or-search
bindkey '^[[B' down-line-or-search

# emacs keybindings
bindkey -e

# make ctrl-w behave nicely
autoload -Uz select-word-style && select-word-style bash

# enable completion
zstyle :compinstall filename ~/.zshrc
autoload -Uz compinit
compinit -C

# case insensitive completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' \
	'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# syntax highlighting
if [[ -d ~/.zsh/plugins/zsh-syntax-highlighting ]]; then
	ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
	source ~/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# ls colors
if command -v dircolors >/dev/null; then
	# gnu ls
	eval $(dircolors ~/.zsh/dircolors.256dark)
	alias ls='ls --color=auto'
else
	# bsd ls
	export CLICOLOR=1
fi

# other
setopt RM_STAR_SILENT PROMPT_SUBST EXTENDED_GLOB
export EDITOR=vim

source ~/.zsh/aliases.zsh
source ~/.zsh/prompt.zsh
source ~/.zsh/python.zsh

# anything that modifies PATH should be before or inside path.zsh
source ~/.zsh/path.zsh

